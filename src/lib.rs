use nalgebra as na;

fn barycentric_coords(verts: &[Vertex; 3], x: f32, y: f32) -> na::Point3<f32> {
  let x0 = verts[0].pos.x;
  let y0 = verts[0].pos.y;
  let x1 = verts[1].pos.x;
  let y1 = verts[1].pos.y;
  let x2 = verts[2].pos.x;
  let y2 = verts[2].pos.y;

  // Get edge vectors AB, AC, and PA as ints
  let abx = x1 - x0;
  let aby = y1 - y0;
  let acx = x2 - x0;
  let acy = y2 - y0;
  let pax = x0 - x;
  let pay = y0 - y;

  // Cross product vector (abx, acx,pax) against (aby, acy, pay)
  let cross_x = acx * pay - acy * pax;
  let cross_y = aby * pax - abx * pay;
  let cross_z = abx * acy - aby * acx;

  // If Z is 0 then the point or triangle is bad, so just return a coord
  // outside of the triangle
  if cross_z == 0.0 {
    return na::Point3::new(-1.0, -1.0, -1.0);
  }

  // Determine barycentric coords t, u, and v by scaling cross product such
  // that its Z component is 1
  let u = cross_x / cross_z;
  let v = cross_y / cross_z;
  let t = 1.0 - (u + v);

  na::Point3::new(t, u, v)
}

pub struct Renderer<'a, VS, FS, Fragment, Uniforms>
where
  VS: VertexShader<Uniforms>,
  FS: FragmentShader<Fragment, Uniforms>,
{
  pub buf: &'a mut Framebuffer<Fragment>,
  pub vs: VS,
  pub fs: FS,
  pub _pd: std::marker::PhantomData<Uniforms>,
}

impl<'a, VS, FS, Fragment, Uniforms> Renderer<'a, VS, FS, Fragment, Uniforms>
where
  VS: VertexShader<Uniforms>,
  FS: FragmentShader<Fragment, Uniforms>,
{
  pub fn begin(buf: &'a mut Framebuffer<Fragment>, vs: VS, fs: FS) -> Self {
    Self {
      buf,
      vs,
      fs,
      _pd: std::marker::PhantomData,
    }
  }

  pub fn draw_triangle(&mut self, verts: &[Vertex; 3], uniforms: &Uniforms) {
    // Apply vertex shader
    let mut verts = [
      self.vs.apply(&verts[0], uniforms),
      self.vs.apply(&verts[1], uniforms),
      self.vs.apply(&verts[2], uniforms),
    ];

    // We want to check for vertices that are behind the camera, so we can correctly clip them and
    // avoid geometric catastrophy.
    let mut oob_count = 0;
    let mut oob_list = [ false; 3 ];
    for i in 0..3 {
      if verts[i].pos.w >= 0.0 {
        oob_count += 1;
        oob_list[i] = true;
      }
    }

    // There's a more legit way to approach this, but realistically there are only 4 cases that
    // need handling.
    match oob_count {
      // If no verts are out of bounds then we can just rasterise the triangle
      0 => self.rasterise_triangle(verts, uniforms),

      // If only one vert is out of bounds then we have to render a rhombus. This is the most
      // annoying case as we have to create 2 triangles  to render
      1 => {
        let oob_vert_idx = oob_list.iter().cloned().position(|b| b).unwrap();
        let oob_vert = verts[oob_vert_idx].clone();
        let ib_verts = match oob_vert_idx {
          0 => [verts[1].clone(), verts[2].clone()],
          1 => [verts[0].clone(), verts[2].clone()],
          2 => [verts[0].clone(), verts[1].clone()],
          _ => unreachable!("Only 3 verts per triangle"),
        };

        let oa = ib_verts[0].pos - oob_vert.pos;
        let ob = ib_verts[1].pos - oob_vert.pos;
        let tex_oa = ib_verts[0].tex - oob_vert.tex;
        let tex_ob = ib_verts[1].tex - oob_vert.tex;

        let ai = oob_vert.pos + oa * ((-1.0 - oob_vert.pos.z) / oa.z);
        let bi = oob_vert.pos + ob * ((-1.0 - oob_vert.pos.z) / ob.z);
        let tex_ai = oob_vert.tex + tex_oa * ((-1.0 - oob_vert.pos.z) / oa.z);
        let tex_bi = oob_vert.tex + tex_ob * ((-1.0 - oob_vert.pos.z) / ob.z);


        let mut ai_vert = oob_vert.clone();
        ai_vert.pos = ai;
        ai_vert.tex = tex_ai;

        let mut bi_vert = oob_vert.clone();
        bi_vert.pos = bi;
        bi_vert.tex = tex_bi;

        let tri1 = [
          ai_vert.clone(),
          bi_vert.clone(),
          ib_verts[0].clone(),
        ];

        let tri2 = [
          bi_vert.clone(),
          ib_verts[0].clone(),
          ib_verts[1].clone(),
        ];
        self.rasterise_triangle(tri1, uniforms);
        self.rasterise_triangle(tri2, uniforms);
      }

      // If two verts are out of bounds then we just have to move them to their respective edges'
      // intersection point with the bounding plane and we can rasterise a single triangle
      2 => {
        let in_bound_vert = oob_list.iter().cloned().position(|b| !b).unwrap();
        for i in 0..3 {
          if i == in_bound_vert {
            continue; 
          }
          
          let diff = verts[in_bound_vert].pos - verts[i].pos;
          verts[i].pos += diff * ((-1.0 - verts[i].pos.z) / diff.z);

          let tex_diff = verts[in_bound_vert].tex - verts[i].tex;
          verts[i].tex += tex_diff * ((-1.0 -verts[i].pos.z) / diff.z);
        }
        self.rasterise_triangle(verts,uniforms);
      }

      3 => (),

      _ => unreachable!("There are only 3 verts"),
    }
  }

  fn rasterise_triangle(&mut self, mut verts: [Vertex; 3], uniforms: &Uniforms) {
    // Convert positions from homogeneous to euclidean coords. Retain the w coordinate for usage
    // when interpolating normals and tex coords.
    verts[0].pos.x /= verts[0].pos.w;
    verts[0].pos.y /= verts[0].pos.w;
    verts[0].pos.z /= verts[0].pos.w;
    verts[1].pos.x /= verts[1].pos.w;
    verts[1].pos.y /= verts[1].pos.w;
    verts[1].pos.z /= verts[1].pos.w;
    verts[2].pos.x /= verts[2].pos.w;
    verts[2].pos.y /= verts[2].pos.w;
    verts[2].pos.z /= verts[2].pos.w;

    // Scale positions into screen space
    verts[0].pos += na::Vector4::new(1.0, 1.0, 0.0, 0.0);
    verts[0].pos.x *= self.buf.width as f32 / 2.0;
    verts[0].pos.y *= self.buf.height as f32 / 2.0;

    verts[1].pos += na::Vector4::new(1.0, 1.0, 0.0, 0.0);
    verts[1].pos.x *= self.buf.width as f32 / 2.0;
    verts[1].pos.y *= self.buf.height as f32 / 2.0;

    verts[2].pos += na::Vector4::new(1.0, 1.0, 0.0, 0.0);
    verts[2].pos.x *= self.buf.width as f32 / 2.0;
    verts[2].pos.y *= self.buf.height as f32 / 2.0;

    // Determine bounding box, clamped to the section dimensions
    let min_x = f32::min(verts[0].pos.x, f32::min(verts[1].pos.x, verts[2].pos.x));
    let min_x = f32::max(0.0, min_x) as usize;
    let min_x = usize::min(self.buf.width, min_x);

    let max_x = f32::max(verts[0].pos.x, f32::max(verts[1].pos.x, verts[2].pos.x));
    let max_x = f32::max(0.0, f32::min(self.buf.width as f32, max_x)) as usize;
    let max_x = usize::min(self.buf.width, max_x);

    let min_y = f32::min(verts[0].pos.y, f32::min(verts[1].pos.y, verts[2].pos.y));
    let min_y = f32::max(0.0, min_y) as usize;
    let min_y = usize::min(self.buf.height, min_y);

    let max_y = f32::max(verts[0].pos.y, f32::max(verts[1].pos.y, verts[2].pos.y));
    let max_y = f32::max(0.0, max_y) as usize;
    let max_y = usize::min(self.buf.height, max_y);

    // If there's nothing in bounds, return early
    if min_x > max_x || min_y > max_y {
      return;
    }

    // Loop over the pixels in the bounding box, attempting to render them if they are within the triangle
    for y in min_y..=max_y {
      for x in min_x..=max_x {
        // If the current pixel's barycentric coords are both positive and sum to 1.0 or
        // less than the current point is within the triangle and we should try to draw it
        let (t, u, v) = {
          let coords = barycentric_coords(&verts, x as f32, y as f32);
          (coords.x, coords.y, coords.z)
        };

        if u + v <= 1.0 && u >= 0.0 && v >= 0.0 {
          // Determine depth by interpolating Z across triangle verts
          let z = verts[0].pos.z * t + verts[1].pos.z * u + verts[2].pos.z * v;

          // If the new depth value overwrites the current one, it means we should write
          // to the current fragment
          let idx = x + y * self.buf.width;
          if idx >= self.buf.frags.len() {
            continue;
          }

          if z > self.buf.depth[idx] {
            self.buf.depth[idx] = z;
          } else {
            continue;
          }

          // Interpolating normals and textures is a pain when projection matrices are applied. I
          // don't understand why this maths solves the problem they introduce, but it does.
          let interp_x = t / verts[0].pos.w;
          let interp_y = u / verts[1].pos.w;
          let interp_z = v / verts[2].pos.w;
          let interp_coefficient = 1.0 / (interp_x + interp_y + interp_z);

          let t = interp_coefficient * interp_x;
          let u = interp_coefficient * interp_y;
          let v = interp_coefficient * interp_z;

          // If we updated the depth buffer then we should also draw this pixel
          // Interpolate normal across triangle verts
          let norm_x = verts[0].norm.x * t + verts[1].norm.x * u + verts[2].norm.x * v;
          let norm_y = verts[0].norm.y * t + verts[1].norm.y * u + verts[2].norm.y * v;
          let norm_z = verts[0].norm.z * t + verts[1].norm.z * u + verts[2].norm.z * v;

          // Interpolate tex coords across triangle verts
          let tex_x = verts[0].tex.x * t + verts[1].tex.x * u + verts[2].tex.x * v;
          let tex_y = verts[0].tex.y * t + verts[1].tex.y * u + verts[2].tex.y * v;

          // Put together the vertex information for this fragment
          let frag_vertex = Vertex {
            pos: na::Point4::new(x as f32, y as f32, z, 0.0), 
            norm: na::Vector3::new(norm_x, norm_y, norm_z),
            tex: na::Vector2::new(tex_x, tex_y),
          };

          // Run fragment shader and write output to data slice
          let cur_fragment = &self.buf.frags[idx];
          let fragment = self.fs.apply(cur_fragment, &frag_vertex, &uniforms);
          self.buf.frags[idx] = fragment;
        }
      }
    }
  }
}

#[derive(Clone, PartialEq)]
pub struct Vertex {
  pub pos: na::Point4<f32>,
  pub norm: na::Vector3<f32>,
  pub tex: na::Vector2<f32>,
}

pub struct Framebuffer<Fragment> {
  pub width: usize,
  pub height: usize,
  pub frags: Vec<Fragment>,
  pub depth: Vec<f32>,
}

impl<Fragment> Framebuffer<Fragment> {
  pub fn with_fragment<F>(width: usize, height: usize, f: F) -> Self
  where
    F: Fn() -> Fragment,
  {
    let mut frags = Vec::with_capacity(width * height);
    for _ in 0..width * height {
      frags.push((f)());
    }

    Self {
      width,
      height,
      frags,
      depth: vec![0 as f32; width * height],
    }
  }

  pub fn clear_depth(&mut self) {
    for i in 0..self.width * self.height {
      self.depth[i] = f32::NEG_INFINITY;
    }
  }

  pub fn fill_with<F>(&mut self, f: F)
  where
    F: Fn() -> Fragment,
  {
    for i in 0..self.width * self.height {
      self.frags[i] = (f)();
    }
  }
}

impl<Fragment: Clone> Clone for Framebuffer<Fragment> {
  fn clone(&self) -> Self {
    Self {
      width: self.width,
      height: self.height,
      frags: self.frags.clone(),
      depth: self.depth.clone(),
    }
  }

  fn clone_from(&mut self, source: &Self) {
    *self = source.clone();
  }
}

pub trait VertexShader<Uniforms> {
  fn apply(&self, vertex: &Vertex, uniforms: &Uniforms) -> Vertex;
}

pub trait FragmentShader<Fragment, Uniforms> {
  fn apply(&self, fragment: &Fragment, vertex: &Vertex, uniforms: &Uniforms) -> Fragment;
}

impl<Uniforms, VS> VertexShader<Uniforms> for VS
where
  VS: Fn(&Vertex, &Uniforms) -> Vertex,
{
  fn apply(&self, vertex: &Vertex, uniforms: &Uniforms) -> Vertex {
    (self)(vertex, uniforms)
  }
}

impl<Fragment, Uniforms, FS> FragmentShader<Fragment, Uniforms> for FS
where
  FS: Fn(&Fragment, &Vertex, &Uniforms) -> Fragment,
{
  fn apply(&self, fragment: &Fragment, vertex: &Vertex, uniforms: &Uniforms) -> Fragment {
    (self)(fragment, vertex, uniforms)
  }
}
